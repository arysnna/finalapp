import React from 'react'
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Login({navigation}) {
    return (
        <ImageBackground source={require('../images/bg-welcome.png')} style={styles.container} >
            <View style={{paddingHorizontal: 15,}}>
                <View style={styles.nav}>
                    <TouchableOpacity onPress={()=>navigation.replace('WelcomeScreen')}>
                        <Icon name='arrow-left' size={24} style={{color:'white'}} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='dots-vertical' size={24} style={{color:'white'}} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.header}>Welcome{"\n"}Back</Text>
            </View>
            <View style={styles.bubble}>
                <Text style={styles.title}>Sign In</Text>
                <TextInput placeholder={'Email'} style={styles.input} />
                <TextInput placeholder={'Password'} secureTextEntry={true} style={styles.input} />
                <View style={styles.nav}>
                    <TouchableOpacity>
                        <Text style={{color:'#fd4d4d',fontWeight:'bold'}} >Forgot Password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>navigation.replace('HomeScreen')}>
                        <Text style={{fontWeight:'bold'}}>Sign In</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        paddingTop: 30,
        width: '100%',
        height: 388,
    },
    nav: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    header: {
        color: '#ffff',
        fontSize: 40,
        marginTop: 30,
    },
    bubble: {
        height: 393,
        width: '100%',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        paddingVertical: 40,
        paddingHorizontal: 30,
        backgroundColor: '#ffff',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 30,
    },
    input: {
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#fd4d4d',
        padding: 0,
    },
})
