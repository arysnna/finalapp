import React from 'react'
import { StyleSheet, Text, View, ScrollView, } from 'react-native'

export default function Wish() {
    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View>
                <Text>Wishlist Screen</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
