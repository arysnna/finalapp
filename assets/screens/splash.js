import React, { useEffect } from 'react'
import { StyleSheet, Image, View } from 'react-native'

export default function Splash({navigation}) {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('WelcomeScreen')
        }, 3500)
    })

    return (
        <View style={styles.container}>
            <Image source={require('../images/splash2.png')} style={styles.img} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    img: {
        flex: 1,
        width: '100%'
    }
})
