import React, { useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

export default function Account() {
    return (
        <View style={styles.container}>
            <Text>Account Screen</Text>
            <Button title='Log Out' />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
