import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Register({navigation}) {
    const [myNumber, setNumber] = useState(0)
    const onTextChanged = (e) => {
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < e.length; i++) {
            if(numbers.indexOf(e[i]) > -1 ) {
                newText = newText + e[i];
            }
            else {
                alert("please enter numbers only");
            }
        }
        setNumber({myNumber: newText}) 
      }
      
    return (
        <ImageBackground source={require('../images/bg-welcome.png')} style={styles.container} >
            <View style={{paddingHorizontal: 15,}}>
                <View style={styles.nav}>
                    <TouchableOpacity onPress={()=>navigation.replace('WelcomeScreen')}>
                        <Icon name='arrow-left' size={24} style={{color:'white'}} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='dots-vertical' size={24} style={{color:'white'}} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.header}>Lets Start</Text>
            </View>
            <View style={styles.bubble}>
                <Text style={styles.title}>Sign Up</Text>
                <TextInput placeholder={'Name'} style={styles.input} />
                <TextInput placeholder={'Email'} style={styles.input} />
                <TextInput placeholder={'Phone Number'} 
                    keyboardType='numeric'
                    maxLength={14}  
                    onChangeText = {(e)=> onTextChanged(e)}
                    style={styles.input}
                />
                <TextInput placeholder={'Password'} secureTextEntry={true} style={styles.input} />
                <View style={styles.nav}>
                    <TouchableOpacity onPress={()=>navigation.replace('LoginScreen')}>
                        <Text style={{color:'#fd4d4d'}} >Already Have an Account?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text >Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        paddingTop: 30,
        width: '100%',
        height: 380,
    },
    nav: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    header: {
        color: '#ffff',
        fontSize: 40,
        marginTop: 30,
    },
    bubble: {
        height: 402,
        width: '100%',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        paddingVertical: 40,
        paddingHorizontal: 30,
        backgroundColor: '#ffff',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 30,
    },
    input: {
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#fd4d4d',
        padding: 0,
    },
})
