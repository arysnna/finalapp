import React from 'react'
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity } from 'react-native'

export default function Welcome({navigation}) {
    return (
        <ImageBackground source={require('../images/bg-welcome.png')} style={styles.container} >
            <Text style={styles.welcome}>Welcome</Text>
            <View style={styles.bubble}>
                <TouchableOpacity style={styles.button1} onPress={()=>navigation.replace('RegisterScreen')} >
                    <Text style={styles.buttonText1}>Register</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button2} onPress={()=>navigation.replace('LoginScreen')}>
                    <Text style={styles.buttonText2}>Log In</Text>
                </TouchableOpacity>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        width: '100%',
        height: 577,
    },
    welcome: {
        color: '#ffff',
        marginTop: 140,
        marginLeft: 30,
        fontSize: 48,
        fontFamily: 'Roboto',
    },
    bubble: {
        height: 177,
        width: '100%',
        borderRadius: 20,
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffff',
    },
    button1: {
        height: 44,
        width: 343,
        backgroundColor: '#fd4d4d',
        borderWidth: 0,
        borderColor: 'red',
        borderRadius: 15,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button2: {
        height: 44,
        width: 343,
        backgroundColor: '#ffff',
        borderWidth: 1,
        borderColor: 'red',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText1: {
        color: '#ffff',
    },
    buttonText2: {
        color: '#fd4d4d',
    },
})
