import React from 'react'
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, } from 'react-native'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'

export default function Home() {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Image source={require('../images/home-banner.png')} style={styles.banner}/>
            <View style={styles.group1}>
                <View style={styles.saldo}>
                    <Text>Saldo</Text>
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.group1menu}>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <Ionicons name={'ios-expand'} size={30} color={'green'}/>
                                </View>
                                <Text style={styles.group1optitle}>Lihat Semua</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <MaterialCommunityIcons name={'truck'} size={30} color={'#F2C94C'}/>
                                </View>
                                <Text style={styles.group1optitle}>Gratis Ongkir Xtra</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <MaterialCommunityIcons name={'reload'} size={30} color={'#EB5757'}/>
                                </View>
                                <Text style={styles.group1optitle}>TopUp & Tagihan</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <Ionicons name={'ios-pin'} size={30} color={'green'}/>
                                </View>
                                <Text style={styles.group1optitle}>Deals di Sekitar</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <MaterialCommunityIcons name={'cash'} size={30} color={'#F2994A'}/>
                                </View>
                                <Text style={styles.group1optitle}>Cashback & Voucher</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.group1option}>
                                <View style={styles.group1bubble}>
                                    <MaterialCommunityIcons name={'airplane'} size={30} color={'purple'}/>
                                </View>
                                <Text style={styles.group1optitle}>Travel & Entertain</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
            <Image source={require('../images/home-banner2.png')} style={styles.banner2}/>
            <View style={styles.group2}>
                <Text style={{color:'#FF3D71',fontWeight:'bold',fontSize:18}}>Flash Sell</Text>
            </View>
            <View style={styles.group2}>
                <Text style={{color:'#FF3D71',fontWeight:'bold',fontSize:18}}>Popular Product</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    banner: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
    },
    banner2: {
        width: '100%',
        height: 100,
        resizeMode: 'cover',
    },
    group1: {
        backgroundColor: '#255BA5'
    },
    saldo: {
        height: 50,
        width: '95%',
        borderRadius: 10,
        marginVertical: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F4F4F4',
    },
    group1menu: {
        height: 90,
        paddingLeft: 10,
        display: 'flex',
        flexDirection: 'row',
    },
    group1option: {
        flexDirection: 'column',
        alignItems: 'center', 
        marginRight: 18,
        width: 55,
    },
    group1optitle:{
        color:'white',textAlign: 'center',fontWeight:'200',
        fontSize: 12,
    },
    group1bubble: {
        width: 45,
        height: 45,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    group2: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: 5,
        height: 160,
        paddingHorizontal: 10,
    },
})
