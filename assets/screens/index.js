import Account from './account'
import Details from './details'
import Home from './home'
import Login from './login'
import Register from './register'
import Splash from './splash'
import Welcome from './welcome'
import Wish from './wish'

export { Account, Details, Home, Login, Register, Splash, Welcome, Wish, }