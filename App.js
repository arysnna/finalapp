import React from 'react';
import Index from './assets/index';
import { NavigationContainer } from '@react-navigation/native'
import { StyleSheet, Text, View } from 'react-native'
import firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyDUGoi2Y_EvuxTPsxX79hG7kfbko7YQ4RY",
  authDomain: "finalapp-26339.firebaseapp.com",
  databaseURL: "https://finalapp-26339.firebaseio.com",
  projectId: "finalapp-26339",
  storageBucket: "finalapp-26339.appspot.com",
  messagingSenderId: "749875441267",
  appId: "1:749875441267:web:07fe694b48d4db01de4406",
  measurementId: "G-WEJKQY4G8Z"
};
firebase.initializeApp(firebaseConfig);

export default function App() {
  return (
    <NavigationContainer style={styles.container}>
      <Index />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
