import React from 'react'
import { Account, Details, Home, Login, Register, Splash, Welcome, Wish, } from './screens'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons } from '@expo/vector-icons'

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (
    <Tabs.Navigator 
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
            let iconName
            if (route.name === 'Home') {
                iconName = focused ? 'ios-home' : 'ios-home';
            }else if (route.name === 'Wishlist') {
                iconName = focused ? 'ios-heart' : 'ios-heart-empty';
            }else if (route.name === 'Profiles') {
                iconName = focused ? 'ios-person' : 'ios-person';
            }
            return <Ionicons name={iconName} size={size} color={color} style={{margin:0, padding:0}} />
            },
        })} 
        tabBarOptions={{
            activeTintColor: 'red',
            inactiveTintColor: 'gray',
            style: {
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#ffff',
                height: 60,
                borderRadius: 10,
            }
        }}
    >
        <Tabs.Screen name='Home' component={Home} />
        <Tabs.Screen name='Wishlist' component={Wish} />
        <Tabs.Screen name='Profiles' component={Account} />
    </Tabs.Navigator>
)

export default() => (
    <AuthStack.Navigator>
        <AuthStack.Screen name='SplashScreen' component={Splash} options={{ headerShown: false }} />
        <AuthStack.Screen name='HomeScreen' component={TabsScreen} options={{ headerShown: false }} />
        <AuthStack.Screen name='RegisterScreen' component={Register} options={{ headerShown: false }} />
        <AuthStack.Screen name='WelcomeScreen' component={Welcome} options={{ headerShown: false }} />
        <AuthStack.Screen name='LoginScreen' component={Login} options={{ headerShown: false }} />
        <AuthStack.Screen name='DetailsScreen' component={Details} options={{ headerShown: false }} />
    </AuthStack.Navigator>
)